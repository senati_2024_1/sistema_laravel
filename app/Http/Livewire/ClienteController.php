<?php

namespace App\Http\Livewire;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index()
    {
        $clientes = Cliente::all();
        return view('clientes.index', compact('clientes'));
    }

    public function create()
    {
        return view('clientes.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'cod_cli' => 'required|unique:clientes,cod_cli',
            'rso_cli' => 'required',
            'dir_cli' => 'required',
            'tlf_cli' => 'required',
            'cod_dis' => 'required',
            'fec_reg' => 'required|date',
            'tip_cli' => 'required',
            'con_cli' => 'required',
        ]);

        Cliente::create($request->all());

        return redirect()->route('clientes.index')
            ->with('success', 'Cliente creado correctamente.');
    }

    public function show($id)
    {
        $cliente = Cliente::findOrFail($id);
        return view('clientes.show', compact('cliente'));
    }

    public function edit($id)
    {
        $cliente = Cliente::findOrFail($id);
        return view('clientes.edit', compact('cliente'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'rso_cli' => 'required',
            'dir_cli' => 'required',
            'tlf_cli' => 'required',
            'cod_dis' => 'required',
            'fec_reg' => 'required|date',
            'tip_cli' => 'required',
            'con_cli' => 'required',
        ]);

        $cliente = Cliente::findOrFail($id);
        $cliente->update($request->all());

        return redirect()->route('clientes.index')
            ->with('success', 'Cliente actualizado correctamente.');
    }

    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->delete();

        return redirect()->route('clientes.index')
            ->with('success', 'Cliente eliminado correctamente.');
    }
}
