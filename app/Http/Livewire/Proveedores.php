<?php

namespace App\Http\Livewire;

use DB;
use App\Http\Requests\ProveedorRequest;
use App\Models\Proveedor;
use Illuminate\Support\Facades\Gate;
use Livewire\Attributes\Title;
use Livewire\Component;
use Livewire\WithPagination;

class Proveedores extends Component
{
    use WithPagination;

    public $COD_PRV, $RSO_PRV, $DIR_PRV, $TEL_PRV, $REP_PRV, $proveedor_id;
    public $agregarProveedor = false, $actualizarProveedor = false, $eliminarProveedor = false;

    protected $listeners = ['render'];

    #[Title('Proveedores')]
    public function rules()
    {
        return ProveedorRequest::rules($this->proveedor_id);
    }

    public function resetFields()
    {
        $this->RSO_PRV = '';
        $this->DIR_PRV = '';
        $this->TEL_PRV = '';
        $this->REP_PRV = '';
    }

    public function resetValidationAndFields()
    {
        $this->resetValidation();
        $this->resetFields();
        $this->agregarProveedor = false;
        $this->actualizarProveedor = false;
        $this->eliminarProveedor = false;
    }

    public function mount()
    {
        if (Gate::denies('proveedor_index')) {
            return redirect()->route('dashboard')
                ->with('message', trans('message.No tienes los permisos necesarios para ejecutar esta acción.'))
                ->with('alert_class', 'danger');
        }
    }

    public function render()
    {
        $proveedores = Proveedor::orderBy('RSO_PRV', 'asc')->paginate(10);
        return view('proveedores.index', compact('proveedores'));
    }

    public function create()
    {
        if (Gate::denies('proveedor_add')) {
            return redirect()->route('proveedores')
                ->with('message', trans('message.No tienes los permisos necesarios para ejecutar esta acción.'))
                ->with('alert_class', 'danger');
        }
        $this->resetValidationAndFields();
        $this->proveedor_id = '';
        $this->agregarProveedor = true;
        return view('proveedores.create');
    }

    public function store()
    {
        if (Gate::denies('proveedor_add')) {
            return redirect()->route('proveedores')
                ->with('message', trans('message.No tienes los permisos necesarios para ejecutar esta acción.'))
                ->with('alert_class', 'danger');
        }
        $this->validate();

        DB::beginTransaction();
        $proveedor = Proveedor::create([
            'RSO_PRV' => $this->RSO_PRV,
            'DIR_PRV' => $this->DIR_PRV,
            'TEL_PRV' => $this->TEL_PRV,
            'REP_PRV' => $this->REP_PRV,
        ]);
        $proveedor->save();
        DB::commit();

        session()->flash('message', 'Proveedor creado exitosamente.');
        $this->emit('render'); // Emitir evento para renderizar la vista
        $this->resetValidationAndFields();
    }

    public function edit($id)
    {
        if (Gate::denies('proveedor_edit')) {
            return redirect()->route('proveedores')
                ->with('message', trans('message.No tienes los permisos necesarios para ejecutar esta acción.'))
                ->with('alert_class', 'danger');
        }

        $proveedor = Proveedor::find($id);

        if (!$proveedor) {
            return redirect()->route('proveedores')
                ->with('message', 'Proveedor no encontrado.')
                ->with('alert_class', 'danger');
        }
        $this->resetValidationAndFields();
        $this->proveedor_id = $proveedor->COD_PRV;
        $this->RSO_PRV = $proveedor->RSO_PRV;
        $this->DIR_PRV = $proveedor->DIR_PRV;
        $this->TEL_PRV = $proveedor->TEL_PRV;
        $this->REP_PRV = $proveedor->REP_PRV;
        $this->actualizarProveedor = true;
        return view('proveedores.edit');
    }

    public function update()
    {
        if (Gate::denies('proveedor_edit')) {
            return redirect()->route('proveedores')
                ->with('message', trans('message.No tienes los permisos necesarios para ejecutar esta acción.'))
                ->with('alert_class', 'danger');
        }

        $this->validate();

        $proveedor = Proveedor::find($this->proveedor_id);
        if (!$proveedor) {
            return redirect()->route('proveedores')
                ->with('message', 'Proveedor no encontrado.')
                ->with('alert_class', 'danger');
        }
        DB::beginTransaction();
        $proveedor->RSO_PRV = $this->RSO_PRV;
        $proveedor->DIR_PRV = $this->DIR_PRV;
        $proveedor->TEL_PRV = $this->TEL_PRV;
        $proveedor->REP_PRV = $this->REP_PRV;
        $proveedor->save();
        DB::commit();

        session()->flash('message', 'Proveedor actualizado exitosamente.');
        $this->emit('render'); // Emitir evento para renderizar la vista
        $this->resetValidationAndFields();
    }

    public function cancel()
    {
        $this->resetValidationAndFields();
    }

    public function setDeleteId($id)
    {
        if (Gate::denies('proveedor_delete')) {
            return redirect()->route('proveedores')
                ->with('message', trans('message.No tienes los permisos necesarios para ejecutar esta acción.'))
                ->with('alert_class', 'danger');
        }

        $proveedor = Proveedor::find($id);
        if (!$proveedor) {
            return redirect()->route('proveedores')
                ->with('message', 'Proveedor no encontrado.')
                ->with('alert_class', 'danger');
        }
        $this->proveedor_id = $proveedor->COD_PRV;
        $this->resetValidationAndFields();
        $this->eliminarProveedor = true;
    }

    public function delete()
    {
        if (Gate::denies('proveedor_delete')) {
            return redirect()->route('proveedores')
                ->with('message', trans('message.No tienes los permisos necesarios para ejecutar esta acción.'))
                ->with('alert_class', 'danger');
        }

        $proveedor = Proveedor::find($this->proveedor_id);
        if (!$proveedor) {
            return redirect()->route('proveedores')
                ->with('message', 'Proveedor no encontrado.')
                ->with('alert_class', 'danger');
        }
        DB::beginTransaction();
        $proveedor->delete();
        DB::commit();

        session()->flash('message', 'Proveedor eliminado exitosamente.');
        $this->emit('render'); // Emitir evento para renderizar la vista
        $this->resetValidationAndFields();
    }
}
