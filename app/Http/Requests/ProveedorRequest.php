<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProveedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $proveedorId = $this->route('proveedor') ? $this->route('proveedor')->cod_prv : null;

        $baseRules = [
            'RSO_PRV' => ['required', 'string', 'max:80'],
            'DIR_PRV' => ['required', 'string', 'max:100'],
            'TEL_PRV' => ['nullable', 'string', 'max:15'],
            'REP_PRV' => ['required', 'string', 'max:80'],
            'COD_DIS' => ['required', 'exists:DISTRITO,COD_DIS'],
        ];

        if ($proveedorId) {
            $baseRules['RSO_PRV'][] = 'unique:dbo.PROVEEDOR,RSO_PRV,' . $proveedorId . ',cod_prv';
            //$baseRules['TEL_PRV'][] = 'unique:dbo.PROVEEDOR,TEL_PRV,' . $proveedorId . ',cod_prv';
        } else {
            $baseRules['RSO_PRV'][] = 'unique:dbo.PROVEEDOR,RSO_PRV';
            //$baseRules['TEL_PRV'][] = 'unique:dbo.PROVEEDOR,TEL_PRV';
        }

        return $baseRules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        return [
            'RSO_PRV' => 'razón social',
            'DIR_PRV' => 'dirección',
            'TEL_PRV' => 'teléfono',
            'REP_PRV' => 'representante',
            'COD_DIS' => 'distrito',
        ];
    }
}

