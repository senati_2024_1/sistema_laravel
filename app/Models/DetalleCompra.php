<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleCompra extends Model
{
    use HasFactory;

    protected $table = 'detalle_compras';
    protected $primaryKey = ['num_oco', 'cod_pro'];
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['num_oco', 'cod_pro', 'can_det'];

    public function ordenCompra()
    {
        return $this->belongsTo(OrdenCompra::class, 'num_oco', 'num_oco');
    }

    public function producto()
    {
        return $this->belongsTo(Producto::class, 'cod_pro', 'cod_pro');
    }

    protected function setKeysForSaveQuery($query)
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $key) {
            $query->where($key, '=', $this->getKeyForSaveQuery($key));
        }

        return $query;
    }

    protected function getKeyForSaveQuery($key = null)
    {
        if (is_null($key)) {
            $key = $this->getKeyName();
        }

        return $this->getAttribute($key);
    }
}
