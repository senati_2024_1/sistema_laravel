<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Abastecimiento extends Model
{
    use HasFactory;

    protected $table = 'abastecimientos';
    protected $primaryKey = ['cod_prv', 'cod_pro'];
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['cod_prv', 'cod_pro', 'pre_aba'];

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class, 'cod_prv', 'cod_prv');
    }

    public function producto()
    {
        return $this->belongsTo(Producto::class, 'cod_pro', 'cod_pro');
    }
}
