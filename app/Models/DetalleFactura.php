<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleFactura extends Model
{
    use HasFactory;

    protected $table = 'detalle_facturas';
    protected $primaryKey = ['num_fac', 'cod_pro'];
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['num_fac', 'cod_pro', 'can_ven', 'pre_ven'];

    public function factura()
    {
        return $this->belongsTo(Factura::class, 'num_fac', 'num_fac');
    }

    public function producto()
    {
        return $this->belongsTo(Producto::class, 'cod_pro', 'cod_pro');
    }

    protected function setKeysForSaveQuery($query)
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $key) {
            $query->where($key, '=', $this->getKeyForSaveQuery($key));
        }

        return $query;
    }

    protected function getKeyForSaveQuery($key = null)
    {
        if (is_null($key)) {
            $key = $this->getKeyName();
        }

        return $this->getAttribute($key);
    }
}
