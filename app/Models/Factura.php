<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    use HasFactory;

    protected $table = 'facturas';
    protected $primaryKey = 'num_fac';
    public $incrementing = false;
    protected $keyType = 'char';
    protected $fillable = ['num_fac', 'fec_fac', 'cod_cli', 'fec_can', 'est_fac', 'cod_ven', 'por_igv'];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'cod_cli', 'cod_cli');
    }

    public function vendedor()
    {
        return $this->belongsTo(Vendedor::class, 'cod_ven', 'cod_ven');
    }

    public function detalleFacturas()
    {
        return $this->hasMany(DetalleFactura::class, 'num_fac', 'num_fac');
    }
}
