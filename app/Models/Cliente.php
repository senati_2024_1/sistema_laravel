<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $table = 'clientes';
    protected $primaryKey = 'cod_cli';
    public $incrementing = false;
    protected $keyType = 'char';
    protected $fillable = ['cod_cli', 'rso_cli', 'dir_cli', 'tlf_cli', 'ruc_cli', 'cod_dis', 'fec_reg', 'tip_cli', 'con_cli'];

    public function distrito()
    {
        return $this->belongsTo(Distrito::class, 'cod_dis', 'cod_dis');
    }

    public function facturas()
    {
        return $this->hasMany(Factura::class, 'cod_cli', 'cod_cli');
    }
}
